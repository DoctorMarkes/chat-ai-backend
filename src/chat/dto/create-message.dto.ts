export class CreateMessageDto {
  readonly id: string;
  readonly message: string;
}
