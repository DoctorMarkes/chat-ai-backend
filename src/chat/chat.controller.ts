import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ChatService } from './chat.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { ObjectId } from 'mongoose';
import { AuthRequest } from './auth.middleware';

@Controller('/chats')
export class ChatController {
  constructor(private chatService: ChatService) {}

  @Get()
  getAll(@Req() req: AuthRequest, @Query('offsetPage') offsetPage?: string) {
    const userId = req.userId;

    return this.chatService.getAll(Number(offsetPage), userId);
  }

  @Get('/count')
  getAllCount(@Req() req: AuthRequest) {
    const userId = req.userId;

    return this.chatService.getAllCount(userId);
  }

  @Post()
  addMessage(@Req() req: AuthRequest, @Body() dto: CreateMessageDto) {
    const userId = req.userId;

    return this.chatService.addMessage(dto, userId);
  }

  @Get(':id')
  getOne(@Req() req: AuthRequest, @Param('id') id: ObjectId) {
    const userId = req.userId;

    return this.chatService.getOne(id, userId);
  }

  @Delete(':id')
  archive(
    @Req() req: AuthRequest,
    @Param('id') id: ObjectId,
  ): Promise<ObjectId> {
    const userId = req.userId;

    return this.chatService.archive(id, userId);
  }
}
