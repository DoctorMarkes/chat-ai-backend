import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Chat, ChatDocument } from './schemas/chat.schema';
import { v4 as uuidv4 } from 'uuid';
import { CreateMessageDto } from './dto/create-message.dto';
import { Message, MessageDocument } from './schemas/message.schema';
import OpenAI from 'openai';
import { ChatCompletionMessageParam } from 'openai/resources';
import 'dotenv/config';

const openai = new OpenAI();

@Injectable()
export class ChatService {
  constructor(
    @InjectModel(Chat.name) private chatModel: Model<ChatDocument>,
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
  ) {}

  async getAll(offsetPage = 0, userId: string): Promise<Chat[]> {
    const count = 20;
    return this.chatModel
      .find({ userId: userId, isArchived: false })
      .sort({ updatedAt: -1 })
      .skip(offsetPage)
      .limit(count);
  }

  async getAllCount(userId: string): Promise<number> {
    return this.chatModel
      .find({ userId: userId, isArchived: false })
      .sort({ updatedAt: -1 })
      .countDocuments();
  }

  async addMessage(dto: CreateMessageDto, userId: string): Promise<Chat> {
    const isUpdate = dto.id !== '';
    const hash = isUpdate ? dto.id : uuidv4();
    const filter = { hash, userId, isArchived: false };
    const update = isUpdate
      ? { updatedAt: Date.now() }
      : {
          title: dto.message,
          hash,
        };
    const options = { upsert: true, new: true, setDefaultsOnInsert: true };
    const chat = await this.chatModel
      .findOneAndUpdate(filter, update, options)
      .populate('messages')
      .exec();

    const message = await this.messageModel.create({
      content: dto.message,
      role: 'user',
      chatId: chat.id,
    });

    chat.messages.push(message);

    if (!isUpdate) {
      const responseTitle = await openai.chat.completions.create({
        model: 'gpt-3.5-turbo',
        messages: [
          {
            role: 'user',
            content:
              'Come up with a title in 3-5 words for the chat where the question is asked: ' +
              dto.message,
          },
        ],
      });

      chat.title = responseTitle.choices[0].message.content.replace(
        /^"(.*)"$/,
        '$1',
      );

      chat.userId = userId;
    }

    const messages = isUpdate
      ? (chat.messages.map((mes) => ({
          role: mes.role,
          content: mes.content,
        })) as ChatCompletionMessageParam[])
      : ([
          {
            role: message.role,
            content: message.content,
          },
        ] as ChatCompletionMessageParam[]);

    const completion = await openai.chat.completions.create({
      model: 'gpt-4-0125-preview',
      messages,
    });

    const completionResponseMessage = completion.choices[0].message;

    const completionMessage = await this.messageModel.create({
      content: completionResponseMessage.content,
      role: completionResponseMessage.role,
      chatId: chat.id,
    });

    chat.messages.push(completionMessage);
    await chat.save();
    return chat;
  }

  async getOne(id: ObjectId, userId: string): Promise<Chat> {
    return this.chatModel
      .findOne({
        hash: id,
        userId,
        isArchived: false,
      })
      .populate('messages');
  }

  async archive(id: ObjectId, userId: string): Promise<ObjectId> {
    const chat = await this.chatModel.findOneAndUpdate(
      { _id: id, userId, isArchived: false },
      { isArchived: true, archivedAt: Date.now() },
    );
    return chat.id;
  }
}
