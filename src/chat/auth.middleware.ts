import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { createClerkClient } from '@clerk/clerk-sdk-node';
import 'dotenv/config';

export interface AuthRequest extends Request {
  userId?: string;
}

const clerkClient = createClerkClient({
  secretKey: process.env.CLERK_SECRET_KEY,
});

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  async use(req: AuthRequest, res: Response, next: NextFunction) {
    let clientToken = '';
    const authorizationHeader = req.headers['authorization'] as string;
    if (authorizationHeader) {
      const tokenParts = authorizationHeader.split(' ');
      if (tokenParts.length === 2 && tokenParts[0].toLowerCase() === 'bearer') {
        clientToken = tokenParts[1];
      }
    }

    try {
      const { isSignedIn, toAuth } = await clerkClient.authenticateRequest({
        headerToken: clientToken,
      });

      if (!isSignedIn) {
        return res.sendStatus(401).json({ message: 'unauthorized' });
      }

      req.userId = toAuth().userId;
    } catch (error) {
      return res.sendStatus(401).json({ message: 'unauthorized' });
    }

    return next();
  }
}
