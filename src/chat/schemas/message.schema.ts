import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Chat } from './chat.schema';
import { ChatCompletionRole } from 'openai/src/resources/chat/completions';

export type MessageDocument = Message & Document;

@Schema()
export class Message {
  @Prop()
  content: string;

  @Prop()
  role: ChatCompletionRole;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' })
  chatId: Chat;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
