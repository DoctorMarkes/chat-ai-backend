import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import * as cookieParser from 'cookie-parser';
import 'dotenv/config';

async function bootstrap() {
  const PORT = process.env.PORT || 5050;
  const app = await NestFactory.create(AppModule);
  const corsOptions: CorsOptions = {
    origin: process.env.FRONTEND_URL,
    credentials: true,
    allowedHeaders:
      'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  };
  app.enableCors(corsOptions);
  app.use(cookieParser());

  await app.listen(PORT, () => console.log(`server started on PORT ${PORT}`));
}

bootstrap();
