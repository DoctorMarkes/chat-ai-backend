import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ChatModule } from './chat/chat.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthMiddleware } from './chat/auth.middleware';
import { ChatController } from './chat/chat.controller';
import 'dotenv/config';

const uri = `mongodb+srv://${process.env.DB_LOGIN}:${process.env.DB_PASSWORD}@cluster0.okk9w79.mongodb.net/`;

@Module({
  imports: [ChatModule, MongooseModule.forRoot(uri)],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(ChatController);
  }
}
